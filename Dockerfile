FROM ros:melodic

RUN mkdir -p /usr/src/Dockerized-ROS-Package/src

# Change directory to /usr/src/Dockerized-ROS-Package/src
WORKDIR /usr/src/Dockerized-ROS-Package/src

# Create a package
RUN catkin_create_pkg obiwan rospy std_msgs

# Change directory to /usr/src/Dockerized-ROS-Package/src/obiwan/src
WORKDIR /usr/src/Dockerized-ROS-Package/src/obiwan/src

# Copy obiwanpy.py Or can copy file from github or gitlab repository
COPY Dockerized-ROS-Package/src/obiwan/src/obiwanpy.py .

# Change directory to /usr/src/Dockerized-ROS-Package
WORKDIR /usr/src/Dockerized-ROS-Package

# Make package
RUN /bin/bash -c "echo 'source /opt/ros/melodic/setup.bash' >> ~/.bashrc; source ~/.bashrc; source /opt/ros/melodic/setup.bash; catkin_make;"

# Execute when container is runing
ENTRYPOINT /bin/bash -c "source devel/setup.bash; roscore & rosrun obiwan obiwanpy.py;"

